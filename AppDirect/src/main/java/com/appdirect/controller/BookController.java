package com.appdirect.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.appdirect.impl.BookService;
import com.appdirect.pojo.Book;
import com.appdirect.pojo.RestResponse;

@RestController
public class BookController {

	private BookService bookService;
	
	@Autowired
	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	@RequestMapping(value = "/book", method = RequestMethod.POST,
			consumes = "application/json",
			produces = "application/json"
			)
	public @ResponseBody ResponseEntity<RestResponse> addBook(@RequestBody Book book){
		String result = bookService.addBook(book);
		
		if(result==null){
			RestResponse rs = new RestResponse();
			rs.setMessage("Input fields not in proper format or book already present.");
			return new ResponseEntity<RestResponse>(rs, HttpStatus.BAD_REQUEST);
		}else{
			RestResponse rs = new RestResponse();
			rs.setMessage(result);
			return new ResponseEntity<RestResponse>(rs, HttpStatus.CREATED);
		}
		
		
	}
	
	@RequestMapping(value = "/book/{bookId}", method = RequestMethod.DELETE,
			consumes = "application/json",
			produces = "application/json")
	public @ResponseBody ResponseEntity<RestResponse> deleteBook(@PathVariable int bookId){
		String result = bookService.deleteBook(bookId);
		RestResponse rs = new RestResponse();
		if(result==null){
			rs.setMessage("Book id not valid.");
			return new ResponseEntity<RestResponse>(rs, HttpStatus.NOT_FOUND);
		}else{
			rs.setMessage(result);
			return new ResponseEntity<RestResponse>(rs, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/book/{bookId}", method = RequestMethod.PUT,
			consumes = "application/json",
			produces = "application/json")
	public @ResponseBody ResponseEntity<RestResponse> updateBook(@PathVariable int bookId, @RequestBody Book book){
		String result = bookService.updateBook(bookId, book);
		if(result==null){
			RestResponse rs = new RestResponse();
			rs.setMessage("Input fields not in proper format or book with this id not present.");
			return new ResponseEntity<RestResponse>(rs, HttpStatus.BAD_REQUEST);
		}else{
			RestResponse rs = new RestResponse();
			rs.setMessage(result);
			return new ResponseEntity<RestResponse>(rs, HttpStatus.CREATED);
		}
	}
	
	@RequestMapping(value = "/book/{bookId}", method = RequestMethod.GET,
			consumes = "application/json",
			produces = "application/json")
	public @ResponseBody ResponseEntity<Book> getBook(@PathVariable int bookId){
		Book book = bookService.getBook(bookId);
		if(book==null){
			return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
		}else{
			return new ResponseEntity<Book>(book, HttpStatus.FOUND);
		}
	}
	
	@RequestMapping(value = "/book", method = RequestMethod.GET,
			consumes = "application/json",
			produces = "application/json")
	public @ResponseBody List<Book> getBookList(){
		List<Book> books = bookService.getBooks();
		return books;
	}
	
	@ExceptionHandler(value = Exception.class)
	void handleBadRequest(Exception exception, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
	}
}
