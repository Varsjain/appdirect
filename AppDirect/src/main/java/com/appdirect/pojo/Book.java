package com.appdirect.pojo;

import javax.validation.constraints.NotNull;

public class Book {

	@NotNull
	private String name;
	private int bookId;
	private String authour;
	private int isdn;
	
	public Book(){}
	
	public Book(String name, String authour, int isdn) {
		super();
		this.name = name;
		this.authour = authour;
		this.isdn = isdn;
	}
	
	public Book(String name, int bookId, String authour, int isdn) {
		super();
		this.name = name;
		this.bookId = bookId;
		this.authour = authour;
		this.isdn = isdn;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getBookId() {
		return bookId;
	}
	
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	
	public String getAuthour() {
		return authour;
	}
	
	public void setAuthour(String authour) {
		this.authour = authour;
	}
	
	public int getIsdn() {
		return isdn;
	}
	
	public void setIsdn(int isdn) {
		this.isdn = isdn;
	}
	
}
