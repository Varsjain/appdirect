package com.appdirect;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice  
@RestController  
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)  
    @ExceptionHandler(value = HttpMessageNotReadableException.class)  
    public String handleException(Exception e){  
        return e.getMessage();  
    }  
  
}
