package com.appdirect;

import java.io.IOException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class OAuthInterceptor implements ClientHttpRequestInterceptor {

	@Override
    public ClientHttpResponse intercept(
            HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {

        HttpHeaders headers = request.getHeaders();
        headers.add("Authorization", "OAuth oauth_consumer_key=\"test-139184\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"1476382057\",oauth_nonce=\"Lgp96I\",oauth_version=\"1.0\",oauth_signature=\"e2NSZIPI%2FXhNmqXTU71di3SJILo%3D\"");
        return execution.execute(request, body);
    }
}
