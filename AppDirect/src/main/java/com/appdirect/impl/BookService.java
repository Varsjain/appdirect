package com.appdirect.impl;

import java.util.List;

import com.appdirect.pojo.Book;

public interface BookService {

	public String addBook(Book book);
	public String deleteBook(int bookId);
	public String updateBook(int bookId, Book book);
	public Book getBook(int bookId);
	public List<Book> getBooks();
}
