package com.appdirect.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.appdirect.OAuthInterceptor;
import com.appdirect.pojo.Book;

@Service("bookService")
public class BookServiceImpl implements BookService {

	public static HashMap<Integer, Book> bookDatabase = new HashMap<Integer, Book>();
	public static int bookCounter;
	
	@Override
	public String addBook(Book book) {
		String message = "";
		if(book != null){
			if(checkBookPresent(book)){
				return null;
			}else{
				
				boolean flag = validateBook(book);
				if(flag){
					int bookId = generateBookId();
					book.setBookId(bookId);
					bookDatabase.put(bookId, book);
					createSubscription();
					message = "Book added successfully! Book Id is "+bookId+".";
				}else{
					return null;
				}
				
			}
		}else{
			return null;
		}
		
		return message;
	}

	@Override
	public String deleteBook(int bookId) {
		Book book = new Book();
		if(bookDatabase.containsKey(bookId)){
			book = bookDatabase.remove(bookId);	
			cancelSubscription();
		}else{
			return null;
		}
		
		return "Deleted Book " + book.getName() + " with book id "+book.getBookId()+" from database.";
	}

	@Override
	public String updateBook(int bookId, Book book) {
		if(bookId!=0){
			if(!bookDatabase.containsKey(bookId)){
				return null;
			}
		}else{
			return null;
		}
		
		if(book!=null){
			boolean flag = validateBook(book);
			if(flag){
				book.setBookId(bookId);
				bookDatabase.put(bookId, book);
				return "Book "+book.getName()+" with book id "+bookId+" updated successfully!";
			}else{
				return null;
			}
		}else{
			return null;
		}
	}

	@Override
	public Book getBook(int bookId) {
		Book book = new Book();
		if(bookDatabase.containsKey(bookId)){
			book = bookDatabase.get(bookId);
		}else{
			return null;
		}
		return book;
	}
	
	@Override
	public List<Book> getBooks() {
		List<Book> books = new ArrayList<Book>();
		Set<Integer> keySet = bookDatabase.keySet();
		for(Integer key : keySet){
			books.add(bookDatabase.get(key));
		}
		return books;
	}

	public int generateBookId(){
		return ++bookCounter;
	}
	
	public boolean checkBookPresent(Book book){
		Set<Integer> keySet = bookDatabase.keySet();
		for(Integer key : keySet){
			if(bookDatabase.get(key).getIsdn() == book.getIsdn()){
				return true;
			}
		}
		return false;
	}
	
	
	private boolean validateBook(Book book){
		if(book.getAuthour()==null || book.getIsdn()==0 || book.getName()==null 
				|| book.getAuthour().trim().isEmpty() || book.getName().trim().isEmpty())
			return false;
		else
			return true;
	}
	
	
	private void createSubscription(){
		String url = "https://www.appdirect.com/api/billing/v1/companies/{companyId}/users/{userId}/subscriptions";
		
		Map<String, Object> uriVariables = new HashMap<String, Object>();
		
		uriVariables.put("companyId", "DummyCompanyId");
		uriVariables.put("userId", "DummyUserId");
		
		String request = "{\"order\": {\"paymentPlanId\": \"541\", \"orderLines\":[{\"unit\": \"USER\", \"quantity\": \"3\"}]}}";
		RestTemplate template = new RestTemplate();
		template.setInterceptors(Collections.singletonList(new OAuthInterceptor()));
		template.postForLocation(url, request, uriVariables);
	}
	
	private void cancelSubscription(){
		String url = "https://www.appdirect.com/api/billing/v1/subscriptions/{subscriptionId}";
		
		Map<String, Object> uriVariables = new HashMap<String, Object>();
		
		uriVariables.put("subscriptionId", "DummySubscriptionId");
		
		RestTemplate template = new RestTemplate();
		template.setInterceptors(Collections.singletonList(new OAuthInterceptor()));
		template.delete(url, uriVariables);
	}
	
}
